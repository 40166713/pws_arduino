
//
//    FILE: PWS_Data_Collector
//  AUTHOR: Adam Kisala/40166713
// VERSION: 1.1
// PURPOSE: Collect data from DHT22 and BMP180, send it over JY-MCU Bluetooth transciever
//    DATE: 2015-03-05
//     URL: https://www.sparkfun.com/tutorials/253
//          https://github.com/adafruit/DHT-sensor-library/blob/master/
//


// import libraries
#include <Wire.h>
#include<DHT.h>
#include "define.h"
#include <SoftwareSerial.h>
#include <Streaming.h>
#include <avr/io.h>

//initalize software serial for bluetooth
SoftwareSerial mySerial(3, 4); //RX, TX

// Initialize DHT sensor for normal 16mhz Arduino
DHT dht(DHTPIN, DHTTYPE);

// Calibration values
int ac1;
int ac2; 
int ac3; 
unsigned int ac4;
unsigned int ac5;
unsigned int ac6;
int b1; 
int b2;
int mb;
int mc;
int md;

//LED values
int led = 13;
boolean _toggle1  = 1;

String GET_DATA_CMD = "GET_DATA";

// b5 is calculated in bmp085GetTemperature(...), this variable is also used in bmp085GetPressure(...)
// so ...Temperature(...) must be called before ...Pressure(...).
long b5; 

//sensor's values variables
short _temperature;
long _pressure;
float _humidity;
float _altitude;
float _p0 = 101000;     // Pressure at sea level (Pa)



void setup()
{  
  Serial.begin(9600);
  mySerial.begin(9600);
  Wire.begin();
  bmp085Calibration();
  dht.begin();
  DDRB = 1<<5; //pinMode(led, OUTPUT); - set pin 13 as an output by shifting bits
  //wait at least 2s for DHT to settle down
  delay(2500);

  cli();  //stop interrupts
  initialiseTimerInterrupt();
  sei(); //allow interrupts
}


void loop()
{
  //reading data in from the bluetooth connection
  while(mySerial.available() > 0){
    String incoming = mySerial.readString();
    if(incoming.equals(GET_DATA_CMD)){
      if(_temperature && _pressure && _altitude && _humidity){
          mySerial << "T" << _temperature << "P" << _pressure << "A" << _altitude << "H" << _humidity << endl;
          //print to cmd serial for debugging
          Serial << "T" << _temperature << "P" << _pressure << "A" << _altitude << "H" << _humidity << endl;
          //toogle LED
          toogleLED();
      }
    } else {
      adjustPressure(incoming);
    }    
  }
}

// Stores all of the bmp085's calibration values into global variables
// Calibration values are required to calculate temp and pressure
// This function should be called at the beginning of the program
void bmp085Calibration()
{
  ac1 = bmp085ReadInt(0xAA);
  ac2 = bmp085ReadInt(0xAC);
  ac3 = bmp085ReadInt(0xAE);
  ac4 = bmp085ReadInt(0xB0);
  ac5 = bmp085ReadInt(0xB2);
  ac6 = bmp085ReadInt(0xB4);
  b1 = bmp085ReadInt(0xB6);
  b2 = bmp085ReadInt(0xB8);
  mb = bmp085ReadInt(0xBA);
  mc = bmp085ReadInt(0xBC);
  md = bmp085ReadInt(0xBE);
}

/**
 * @brief timer1 interrupt 1Hz toggles pin 13 (LED)
 *  generates pulse wave of frequency 4Hz/2 = 2Hz (takes two cycles for full wave- toggle high then toggle low)
 * @param vector to ISR
*/
ISR(TIMER1_COMPA_vect){
  //read values from sensors
  _humidity = dht.readHumidity();
  // Read temperature as Celsius
  _temperature = dht.readTemperature();

  short tempForPressure = bmp085GetTemperature(bmp085ReadUT());
  _pressure = bmp085GetPressure(bmp085ReadUP());

  _altitude = (float)44330 * (1 - pow(((float) _pressure/_p0), 0.190295));
}

void toogleLED(){
  if (_toggle1){
    PORTB = 1<<5; //digitalWrite(13,HIGH);
    _toggle1 = 0;
  }
  else{
    PORTB = 0<<5; //digitalWrite(13,HIGH);
    _toggle1 = 1;
  }
}

void adjustPressure(String incoming){
  //convert incoming string to float
  char floatbuf[32]; // make this at least big enough for the whole string
  incoming.toCharArray(floatbuf, sizeof(floatbuf));
  float f = atof(floatbuf);
  //assign it to current altitude
  _altitude = f;  
  //adjust p0 accordingly to current altitude
  _p0 = _pressure/(pow((1-_altitude/44330), 5.255));
}




